# VR Wave Shooter

## Introduction

This is a VR experiment using WebVR ([A-Frame](https://aframe.io/) with Three.js) to test features and viability as a tool for working with a [companion robot](https://bitbucket.org/rPiBot/core), when paired with additional hardware.

[Watch a demo video here.](https://www.youtube.com/watch?v=kOF7gwqYT7M)

[![Watch a demo video](web-vr-experiment.gif)](https://www.youtube.com/watch?v=kOF7gwqYT7M)

## Overview

The application is a simple wave shooter with procedurally generated levels and waves.  Being procedurally based makes it a case of changing settings to generate waves and levels of varying difficulty, although a limit must be placed on the number of enemies visible before the framerate slows to a sickening crawl.

It makes use of raytraced pointers to select and control the "menu" (which contains only one button), with an invisible object in the distance bound to the rotation of the controllers which acts as a target for the bullets to translate to.

Gun models are modified versions of a freely available [model](https://www.turbosquid.com/3d-models/free-laser-rifle-xcom-3d-model/1129737) to save time building, and to test how well importing models works.  The models are attached to the controllers, rotating and translating as the user's hands move.  They shrink in size to hide when not required. 

Collisions are handled by events every frame, which are perhaps more expensive than required here.  It could be more efficient and possibly more reliable to track vector coordinates in an array and run comparisons on expected paths to detect hits, rather than having collisions detected by intersections every frame.

Enemies are textures attached to boxes to help reduce the number of polygons on screen, which is especially helpful for the Oculus Quest's Snapdragon mobile processor.

Sounds fire on actions and events.  They are played through native JavaScript rather than A-Frame's directional audio, which would only reliably work (at time of development) in the Edge browser - but Edge has no VR headset integration so is limited to 2D with static controllers, for which this application isn't built.

The controllers are handled by the framework, although they're currently hard-coded to Oculus touch controllers only in this case.  Compatibility is available for other controllers, but testing would be required.

The scoreboard and stats are updated after each event that may update them, which is probably excessive for some of the data.  Although the calculations aren't too demanding to handle quickly, every avenue of performance improvement helps, especially on mobile VR headsets.

## Why WebVR over Unity for this test?

Flexibility - not tied to a small subset of VR headset or browser
Cross-platform - the 2D test works in most browsers on every operating system tested
Prototyping - viewing in the browser on a refresh makes testing changes instant
No setup times - the only requirement is to visit a URL in the client's browser

## Drawbacks

Performance ceiling - the Oculus Quest runs at almost the same framerate as an Oculus Rift attached to a dedicated gaming PC.  The Quest runs a Webkit browser, while the PC was running Firefox, which could account for some differences.

## Outcome

There are few barriers to entry and no setup times, although limited to the options available in the A-Frame API (which is being improved).  

Perhaps more suitable at this stage as proof of concept companion apps in terms of pairing with a Python robot application.
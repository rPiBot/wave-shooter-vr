var Audio = {
  play: function(sound){
    // Limited to non-directional audio because only Edge will play A-Frame audio reliably
    if(document.querySelector('#asset-'+sound)){
      document.getElementById('asset-'+sound).cloneNode().play();
    }
  }
}

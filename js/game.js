var Game = {
  add_ammo: function(rows){
    var ammo = (((rows * 2 + 1) * rows) / (Game.level / 2)).toFixed();

    for (var hand in Game.ammo){
      Game.ammo[hand] = ammo;
      document.querySelector('#hud_'+hand+'_ammo').setAttribute('scale', '25 25 25');
    }
  },

  move_enemies: function(initial_enemy_count){
    if(Game.game_started) {
      var enemies = document.querySelectorAll('a-entity[mixin="enemy"]');

      for (var i = 0; i < enemies.length; i++) {
        var amount = 1 * Game.level * Game.enemy_direction;
        var pace = Game.level * 100;

        if(Game.frame % (60 - pace) == 1){
          enemies[i].setAttribute('material', 'src: assets/invader.png; transparent: true;');
        }

        if(Game.frame % (120 - pace * 2) == 1){
          enemies[i].setAttribute('material', 'src: assets/invader-alt.png; transparent: true;');
        }

        if(enemies[i].object3D.position.y > 0){
          enemies[i].object3D.position.y -= Game.level / 10;
        }

        enemies[i].object3D.position.z += Game.level / 5;

        Game.enemy_relative_position += amount;
        enemies[i].object3D.position.x += amount;
      }

      if(Game.frame % (60 - pace) == 1){
        Audio.play('fastinvader'+Game.fastinvader);
        if(Game.fastinvader == 4){
          Game.fastinvader = 1;
        } else {
          Game.fastinvader++;
        }
      }

      Game.frame++;
    }
  },

  fire_weapon: function(hand){
    if(Game.ammo[hand] > 0){
      Audio.play('shoot');

      Game.ammo[hand] -= 1;
      Game.shots[hand] += 1;
      Hud.update_ammo();

      var position1 = new THREE.Vector3();
      var position2 = new THREE.Vector3();

      var flash_target = document.querySelector('#flash-target-'+hand);
      var hand_object = document.querySelector('#'+hand+'-hand');

      var start_point = hand_object.object3D.getWorldPosition(position1);
      var end_point = flash_target.object3D.getWorldPosition(position2);

      var flash = document.createElement('a-entity');
      flash.setAttribute('mixin', 'laser-'+hand);
      flash.setAttribute('class', 'laser');
      flash.setAttribute('id', 'laser'+Date.now());
      flash.setAttribute('animation', 'property: position; from: '+ start_point['x'] + ' ' + (start_point['y'] + 0.08) + ' ' + start_point['z'] + '; to: '+ end_point['x'] + ' ' + end_point['y'] + ' ' + end_point['z'] + '; dur: 2000;');
      flash.setAttribute('animation__2', 'property: geometry.radius; to: 0.1; dur: 75;');

      document.querySelector('a-scene').appendChild(flash);

      window.setTimeout(function() {
        Game.cleanup_unused_entities([flash], true);
      }, 2150);
    } else {
      Audio.play('no-ammo');
    }
  },

  hit: function(target, laser){
    if(Game.game_started = true){
      Game.hits++;
      Audio.play('invaderkilled');
      if(document.querySelector('#'+target.id)){
        target.setAttribute('material', 'color: '+laser.getAttribute('material').color);
        target.setAttribute('animation__2', 'property: scale; to: 0 0 0; dur: 99; easing: linear; loop: 1;');
        window.setTimeout(function() {
          Game.cleanup_unused_entities([target]);
        }, 100);
      }
    }
  },

  check_game_end: function(){
    if(Game.game_started == true && document.querySelectorAll('a-entity[mixin="enemy"]').length == 0){
      Game.game_won();
    }
  },

  game_won: function(){
    Audio.play('tada');
    if(Game.wave < 4){
      Hud.message('WAVE '+(Game.wave + 1)+' OF 5 CLEARED');
      Game.wave++;
    } else{
      Game.wave = 0;
      +(Game.level += 0.1).toFixed(1);
      Hud.message('LEVEL '+ (Game.level*10));
    }
    Game.reset_game();
  },

  game_lost: function(target_enemy){
    Hud.message('GAME OVER');
    Audio.play('explosion');
    Game.game_started = false;
    clearInterval(Game.interval);
    Game.interval = null;

    Hud.update_all();

    document.querySelector('#right-hand').setAttribute('raycaster', 'showLine: true');

    document.querySelector('#barrier').setAttribute('material', 'color: #ff0000');
    document.querySelector('#platform').setAttribute('material', 'color: #ff0000');

    target_enemy.setAttribute('material', 'color: #ff0000');

    other_enemies = document.querySelectorAll('a-entity[mixin="enemy"]:not(#'+target_enemy.id+')');
    for(var i=0; i < other_enemies.length;i++){
      other_enemies[i].setAttribute('animation', 'property: scale; to: 0 0 0; dur: 50;');
    }
    Game.reset_game();
  },

  cleanup_unused_entities: function(entities, skip_checks){
    for(var i=0; i < entities.length;i++){
      entities[i].setAttribute('position', '0 -'+ Game.garbage_position+' -10');
      entities[i].setAttribute('scale', '0 0 0');
      Game.garbage_position+=10;
    }

    window.setTimeout(function() {
        for(var i=0; i < entities.length;i++){
          if (document.querySelector('#'+entities[i].id)){
            document.querySelector('a-scene').removeChild(entities[i]);
            Hud.update_all();

            if(skip_checks){
              Game.check_game_end();
            }
          }
        }
    }, 250);
  },

  reset_game: function(){
    for (var hand in Game.ammo){
      document.querySelector('#laser_rifle_'+hand).setAttribute('animation', 'property: scale; to: 0 0 0; dur: 100');
    }

    Game.game_started = false;
    Game.menu = true;
    clearInterval(Game.interval);
    Game.interval = null;

    document.querySelector('#right-hand').setAttribute('raycaster', 'showLine: true');
    document.querySelector('#right-hand').setAttribute('line', 'color: red');

    document.querySelector('#start').setAttribute('animation', 'property: scale; to: 0.2 0.2 0.2; dur: 250; delay:2000');
    document.querySelector('#start').setAttribute('material', 'color: #4CC3D9;');
  },

  menu: true,
  initial: true,
  enemy_relative_position: 0,
  enemy_direction: 1,
  interval: null,
  start_button_active: false,
  game_started: false,
  wave: 0,
  burst: 100, // LOWER IS FASTER
  level: 0.1,
  garbage_position: 10,
  frame: 0,
  firing_left: null,
  firing_right: null,
  fastinvader: 1,
  shots: { 'left': 0, 'right': 0 },
  ammo: { 'left': 0, 'right': 0 },
  hits: 0
}

var Hud = {
  update_ammo: function(){
    for (var hand in Game.ammo){
      if(Game.ammo[hand] < 10){
        document.querySelector('#hud_'+hand+'_ammo').setAttribute('color', '#ff0000');
      } else {
        document.querySelector('#hud_'+hand+'_ammo').setAttribute('color', '#ffffff');
      }
      document.querySelector('#hud_'+hand+'_ammo').setAttribute('value', Game.ammo[hand]);
    }
  },

  update_all: function(){
    Hud.update_ammo();
    document.querySelector('#hud_enemies_remaining').setAttribute('value', document.querySelectorAll('a-entity[mixin="enemy"]').length);
    document.querySelector('#hud_wave').setAttribute('value', Game.wave+1);
    document.querySelector('#hud_level').setAttribute('value', (Game.level * 10).toFixed());

    document.querySelector('#hud_shots').setAttribute('value', Hud.total_shots());
    document.querySelector('#hud_percentage').setAttribute('value', ((Game.hits/Hud.total_shots()*100) || 0).toFixed(1)+'%');
  },

  total_shots: function(){
    return (Game.shots['left'] || 0) + (Game.shots['right'] || 0);
  },

  message: function(text){
    var hud_message = document.querySelector('#hud_message');

    hud_message.setAttribute('value', text);
    hud_message.setAttribute('animation', 'property: position; from:'+hud_message.getAttribute('data-original-position')+'; to: 0 1 -4; dur: 1000; easing: easeOutBack; loop:1;');

    window.setTimeout(function() {
      hud_message.setAttribute('animation', 'property: position; from: 0 1 -4; to: '+hud_message.getAttribute('data-original-position')+'; dur: 1000; easing: easeInBack;');
    }, 3000);

    window.setTimeout(function() {
      hud_message.setAttribute('value', '');
    }, 4000);
  }
}

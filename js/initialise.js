var Initialise = {
  menu: function(){
    document.querySelector('#title').setAttribute('animation', 'property: position; to: 0 3 -5; dur: 1000; easing:easeOutElastic; delay:1000');
    document.querySelector('#title').setAttribute('animation__2', 'property: rotation; to: 0 0 0; dur: 1250; easing:easeOutElastic; delay:1000');
    document.querySelector('#start').setAttribute('animation__2', 'property: position; to: 0 1.7 -3; dur: 1000; easing:easeOutElastic; delay:1500');
  },

  hud: function(){
    var elements = document.querySelectorAll('.hud_initial');
    var title = document.querySelector('#title');
    title.setAttribute('animation', 'property: position; to: 0 10 -5; dur: 500; delay:0; easing:linear');
    window.setTimeout(function() {
      Game.cleanup_unused_entities([title]);
    }, 1000);

    for (var i in elements){
      if(typeof elements[i] == 'object'){
        elements[i].setAttribute('animation',
        'property: position; to: '+elements[i].getAttribute('data-position')+' dur:1000; easing: easeOutBack');
      }
    }
  },

  prepare_game: function(rows){
    Game.add_ammo(rows);
    Hud.update_all();
    Game.frame = 0;

    for (var hand in Game.ammo){
      document.querySelector('#laser_rifle_'+hand).setAttribute('animation', 'property: scale; to: 0.01 0.01 0.01; dur: 500');
    }

    Game.start_button_active = false;
    Game.menu = false;

    document.querySelector('#right-hand').setAttribute('raycaster', 'showLine: false');

    document.querySelector('#barrier').setAttribute('material', 'color: #003300');
    document.querySelector('#platform').setAttribute('material', 'color: #00ff00');

    document.querySelector('#start').setAttribute('animation', 'property: scale; to: 0 0 0; dur: 500;');

    Game.cleanup_unused_entities(document.querySelectorAll('a-entity[mixin="enemy"]'));

    window.setTimeout(function() {
        Initialise.create_enemies(rows);
    }, 10);
  },

  create_enemies: function(rows){
    var enemy_count = 0;

    for (var row = 1; row <= rows; row++) {
      for (var col = -rows; col <= rows; col++) {
        var entityEl = document.createElement('a-entity');
        entityEl.setAttribute('mixin', 'enemy');
        entityEl.setAttribute('id', 'enemy'+enemy_count);
        entityEl.setAttribute('data-enemy', enemy_count);
        entityEl.setAttribute('animation', 'property: position; to: '+(col * 5)+' ' +(row * row * 1 * (Math.abs(col)))+ ' '+(row * -25)+'; dur: 2000; delay:'+ (enemy_count * 25) +'; easing: easeOutElastic;');

        document.querySelector('a-scene').appendChild(entityEl);
        Hud.update_all();
        enemy_count++;
      }
    }

    window.setTimeout(function() {
        Initialise.position_enemies(rows);
    }, (enemy_count * 35));
  },

  position_enemies: function(rows){
    var positions = [];

    var enemies = document.querySelectorAll('a-entity[mixin="enemy"]');
    var enemies_per_row = enemies.length / rows;

    for (var i = 1; i <= rows; i++) {
      for(var j = 0; j < enemies_per_row; j++){
        positions.push(((j * 2 * 2.5) -(rows * 5)) + ' ' + (i*10 + 1) + ' ' + ((i * -15) -50));
      }
    }

    for (var i = 0; i < enemies.length; i++) {
      enemies[i].setAttribute('animation', 'property: position; to: '+ positions.splice(Math.floor(Math.random()*positions.length), 1)+'; dur: 500; easing:easeInOutBack; loop: 1');
    }

    window.setTimeout(function() {
        Game.game_started = true;
        Game.interval = setInterval(function(){ Game.move_enemies(enemies.length); }, 10);
    }, 750 * rows);
  },
}

window.setTimeout(function() {
    Initialise.menu();
}, 100);

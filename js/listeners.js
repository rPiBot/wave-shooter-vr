AFRAME.registerComponent('trigger-listener-right', {
  init: function () {
    var el = this.el;
    el.addEventListener('triggerdown', function (evt) {
      if (Game.start_button_active == true){
        if(Game.initial == true){
          Initialise.hud();
        }
        Game.initial = false;
        Initialise.prepare_game(Game.wave+1);

      } else if (Game.game_started == true) {
        Game.firing_right = setInterval(function(){ Game.fire_weapon('right'); }, Game.burst);
      }
    });

    el.addEventListener('triggerup', function (evt) {
      clearInterval(Game.firing_right);
      Game.firing_right = null;
    });
  }
});

AFRAME.registerComponent('trigger-listener-left', {
  init: function () {
    var el = this.el;
    el.addEventListener('triggerdown', function (evt) {
      if (Game.game_started == true) {
        Game.firing_left = setInterval(function(){ Game.fire_weapon('left'); }, Game.burst);
      }
    });

    el.addEventListener('triggerup', function (evt) {
      clearInterval(Game.firing_left);
      Game.firing_left = null;
    });
  }
});

AFRAME.registerComponent("enemy-collision", {
  init: function() {
    this.el.addEventListener("collisions", (e)=>{
      if(Game.game_started == true && e.detail.els[0]){
        if(e.target.id == 'barrier' && e.detail.els[0].getAttribute('mixin') == 'enemy'){
          Game.game_lost(e.detail.els[0]);
        } else if(e.target.getAttribute('class') == 'laser' && e.detail.els[0].getAttribute('mixin') == 'enemy'){
          Game.hit(e.detail.els[0], e.target);
        }
      }
    })
  }
});

AFRAME.registerComponent("boundary-collision", {
  init: function() {
    this.el.addEventListener("collisions", (e)=>{
      if(Game.game_started == true && e.detail.els[0]){
        if(e.detail.els[0].getAttribute('mixin') == 'enemy'){
           Game.enemy_direction *= -1;
        }
      }
    })
  }
});

AFRAME.registerComponent('thumbstick-listener', {
  init: function () {
  //  Game.wave = 1;      //TODO REMOVE ME TO START GAME WITH CONTROLLER
  //  Initialise.prepare_game(3); //TODO REMOVE ME TO START GAME WITH CONTROLLER

    var el = this.el;
    el.addEventListener('thumbstickdown', function (evt) {
      Game.reset_game();
    });
  }
});

AFRAME.registerComponent('start-button-listener', {
  schema: {
    color: {default: 'red'}
  },

  init: function () {
    var data = this.data;
    var el = this.el;
    var defaultColor = el.getAttribute('material').color;
    var defaultScale = el.getAttribute('scale');
    defaultScale = defaultScale['x'] + ' ' + defaultScale['y'] + ' ' + defaultScale['z'];

    el.addEventListener('mouseenter', function () {
      if(Game.menu == true){
        Game.start_button_active = true;
        el.setAttribute('material', 'color:'+data.color);
        el.setAttribute('animation', 'property: scale; to: 0.5 0.5 0.5; dur: 200; delay:0; easing: easeOutBack');
      }
    });

    el.addEventListener('mouseleave', function () {
      if(Game.menu == true){
        Game.start_button_active = false;
        el.setAttribute('animation', 'property: scale; to: '+defaultScale+'; dur: 200; delay:0; easing: easeOutBack');
        el.setAttribute('material', 'color:'+defaultColor);
      }
    });
  }
});

AFRAME.register
